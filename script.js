/* Model Analyzer */

((global, $)=>{
  "use strict";

  let result_container
  let fileinput


  let MAPPINGS = {}

  const TYPE_MICROPROCESSOR = 'microprocessor'

  const CATEGORY_LOADING_FILE = 0
  const CATEGORY_PARSING_XML = 1
  const CATEGORY_COMPONENTS = 2
  const CATEGORY_LOGIC_CONNECTIONS = 3
  const CATEGORY_POST_PROCESSING_MASSES = 4
  const CATEGORY_POST_PROCESSING_COMPONENTS = 5
  const CATEGORY_POST_PROCESSING_LOGIC_CONNECTIONS= 6
  const CATEGORY_POST_PROCESSING_COLORS = 7
  const CATEGORY_POST_PROCESSING_MICROCONTROLLERS = 8
  const CATEGORY_STATISTICS = 9
  const CATEGORY_LABELS = ['Loading File', 'Parsing XML', 'Analyzing Components', 'Analyzing Logic Connections', 'Postprocessing Masses', 'Postprocessing Components', 'Postprocessing Logic Connections', 'Postprocessing Colors', 'Postprocessing Microcontrollers','Gathering Statistics']


    $.getJSON('/mappings.json', (data)=>{
      MAPPINGS = data
      log('\n\nMAPPINGS loaded!\n\n')

      let missingImages = []
      for(let ck in data.components){
      	let c = data.components[ck]
      	if(! c.hasOwnProperty('image') || typeof c.image !== 'string' || c.image.length === 0){
      		missingImages.push(c.label)
      	}
      }
      if(missingImages.length > 0){
      	console.warn('Missing images for components: ', missingImages.join(', '))
      }
    })


    $(window).on('load', ()=>{
	    fileinput = $('#file')
	    result_container = $('#result')

	    fileinput.on('change', (evt)=>{
	        checkFileInformation()
	    })
	    checkFileInformation()
    })

	function checkFileInformation(){

	    let file = fileinput.get(0).files[0]
	    if(file){
	        log(file)
	        $('#selection #file-information').html('<span class="file_name">'+file.name + '</span><span class="file_size">' + parseInt(file.size / 10000.0) / 100 + ' MB</span>')
	        $('#selection #analyze-button').removeAttr('disabled')
	    } else {
	        $('#selection #file-information').html('-')
	        $('#selection #analyze-button').attr('disabled', 'true')
	    }
	}

	function analyzeBlob(){
	    if(! fileinput){
	        return console.error('Fileinput is not defined!')
	    }
	    let file = fileinput.get(0).files[0]
	    if(!file){
	        return handleFileError('no file choosen')
	    }
	    startAnalyzingProgress()
	    let fr = new FileReader()
	    fr.onload = (data)=>{
	        handleFileProgress(true)
	        processXML(fr.result).then(()=>{
		        stopAnalyzingProgress()
		        $('#selection').fadeOut(200, ()=>{
		            $('#result-container').fadeIn(200)
		        })
		    })
	    }
	    fr.onerror = (evt)=>{
	      handleFileError(evt)
	    }

	    fr.onprogress = (evt)=>{
	      handleFileProgress(evt)
	    }
	    log('reading blob (' + file.size / 1000 + ' kB)')
	    fr.readAsText(file)
	}

	function processXML(xml){
	  	return new Promise((fulfill, reject)=>{
		    const options = {
		        attributeNamePrefix : "",
		        ignoreAttributes : false,
		        ignoreNameSpace : false,
		        allowBooleanAttributes : true,
		        parseNodeValue : true,
		        parseAttributeValue : false,
		        trimValues: true,
		        parseTrueNumberOnly: false,
		        attrValueProcessor: a => he.decode(a, {isAttributeValue: true}),//default is a=>a
		        tagValueProcessor : a => he.decode(a) //default is a=>a
		    }



		    handleAnalyzingProgress(CATEGORY_PARSING_XML, 0)
		    let json = XMLParser.parse(xml, options);
		    handleAnalyzingProgress(CATEGORY_PARSING_XML, 1)

		    log(json)


		    let compat = checkCompatibility(json)

		    if(compat !== true){
		        handleFileError('not compatible: ' + compat)
		        fulfill()
		        return
		    }


		    buildStats(json).then((html)=>{
	        	result_container.html('')
		    	result_container.append(html)
		    	fulfill()
		    })
		})
	}

	function checkCompatibility(json){
	    if(json.vehicle.data_version !== '3' && json.vehicle.data_version !== '2'){
	    	let conf = confirm('Version not supported. Shall we proceed analyzing (data might be wrong)?')
	        if(conf === false){
	        	return 'data_version not compatible'
	        }
	    }
	    if(typeof json.vehicle !== 'object'){
	        return 'vehicle is not an object'
	    }
	    if(typeof json.vehicle.bodies !== 'object'){
	        return 'vehicle.bodies not an object'
	    }

	    return true
	}

  function buildStats(json){
  	return new Promise((fulfill, reject)=>{

	    let v = json.vehicle

	    let data = {
	      general: [],
	      components: [],
	      logics: [],
	      colors: [],
	      microcontrollers: []
	    }

	    if(v.bodies.body instanceof Array !== true){
	    	v.bodies.body = [v.bodies.body]
	    }


	    let component_count = 0

	    let colors = {}

	    let microprocessors = []

		let components = {}

	    function _components(){
	    	return new Promise((fulfill, reject)=>{
	    		handleAnalyzingProgress(CATEGORY_COMPONENTS, 0)
			    let bodycounter = 0
			    let bodylength = v.bodies.body.length
			    for(let b of v.bodies.body){
			        component_count += (typeof b.components === 'object' && b.components.c instanceof Array ? b.components.c.length : 0)
			        if(b.components.c instanceof Array){
				        for(let c of b.components.c){
				            if (c.d === undefined) {
				                c.d = '01_block'
				            }
				            /* component count */
				            if(typeof components[c.d] !== 'number'){
				                components[c.d] = 1
				            } else {
				                components[c.d]++
				            }

				            /* color bc */
				            if(typeof c.o === 'object' && typeof c.o.bc === 'string' ){
				                if(c.o.bc.length >= 3){
		                    	    let clean = cleanColor(c.o.bc)
					                if(typeof colors[ clean ] !== 'number'){
					                  colors[ clean ] = 1
					                } else {
					                    colors[ clean ]++
					                }
				                } else {
				                	log('found color with less then 3 chars', c, c.o.bc)
				                }
				            }

				            /* color ac Additive (Block) */
				            if(typeof c.o === 'object' && typeof c.o.ac === 'string' ){
					            if(c.o.ac.length >= 3){
					                let clean = cleanColor(c.o.ac)
		                        if(typeof colors[ clean ] !== 'number'){
					                	colors[ clean ] = 1
					                } else {
					                	colors[ clean ]++
					                }
					            } else {
					                log('found color with less then 3 chars', c, c.o.ac)
					            }
				            }

				            /* color sc */
				            if(typeof c.o === 'object' && typeof c.o.sc === 'string' ){
					            let split = c.o.sc.split(',')

					            for(let s of split){
					                if(s.length >= 3){
						                let clean = cleanColor(s)
			                            if(typeof colors[ clean ] !== 'number'){
						                  colors[ clean ] = 1
						                } else {
						                  colors[ clean ]++
						                }
					                } else {
					                	//log('found color with less then 3 chars', c, c.o.sc)
					                }
					            }
				            }

			                /* color ca Additive (Indicator/Sign Pixel) */
			                if(typeof c.o === 'object' && c.o.ca0 instanceof Object === true ){
				                let ca_s = []
				                for(let k of Object.keys(c.o)){
				                    if(typeof k === 'string' && k.match(/ca\d\d/)){
				                      	ca_s.push(c.o[k])
				                    }
				                }

				                for(let ca of ca_s){
				                    if(ca.r && ca.g && ca.b){
				                        let hex = fullRgbToHex(ca.r,ca.g,ca.b)
				                        if(hex){
					                        let clean = cleanColor(hex)
					                        if(typeof colors[ clean ] !== 'number'){
					                            colors[ clean ] = 1
					                        } else {
					                            colors[ clean ]++
					                        }
				                        }
				                    } else {
				                        //log('found color with less then 3 chars', c, c.o.sc)
				                    }
				                }
			                }

			                /* color cc Normal (Indicator/Sign Pixel) */
			                if(typeof c.o === 'object' && c.o.cc0 instanceof Object === true ){
			                    let cc_s = []
			                    for(let k of Object.keys(c.o)){
				                    if(typeof k === 'string' && k.match(/cc\d\d/)){
				                        cc_s.push(c.o[k])
				                    }
			                    }

			                    for(let cc of cc_s){
				                    if(cc.r && cc.g && cc.b){
				                        let hex = fullRgbToHex(cc.r,cc.g,cc.b)
				                        if(hex){
					                        let clean = cleanColor(hex)
					                        if(typeof colors[ clean ] !== 'number'){
					                            colors[ clean ] = 1
					                        } else {
					                            colors[ clean ]++
					                        }
				                        }
				                    } else {
				                        //log('found color with less then 3 chars', c, c.o.sc)
				                    }
			                    }
			                }

				            if(c.d === TYPE_MICROPROCESSOR && c.o.microprocessor_definition instanceof Object ){
				            	microprocessors.push(c.o.microprocessor_definition)
				            }
				        }
			        }
			        bodycounter++;
			        handleAnalyzingProgress(CATEGORY_COMPONENTS, bodycounter / bodylength)
			    }
			    handleAnalyzingProgress(CATEGORY_COMPONENTS, 1)
			    setTimeout(fulfill, 1)
	    	})
	    }

		let total_cable_length = 0
		let logics = {}

	    function _logic_connections(){
	    	return new Promise((fulfill, reject)=>{
				handleAnalyzingProgress(CATEGORY_LOGIC_CONNECTIONS, 0)
				if(v.logic_node_links && v.logic_node_links.logic_node_link instanceof Array){
			      let logiclength = v.logic_node_links.logic_node_link.length
			      let logiccounter = 0
			      for(let l of v.logic_node_links.logic_node_link){
			        if (l.type === undefined) {
			          l.type = "0"
			        }
			        if(typeof logics[l.type] !== 'number'){
			          logics[l.type] = 1
			        } else {
			          logics[l.type]++
			        }

			        try {
			          let cablelength = Math.sqrt( Math.pow(l.voxel_pos_0.x - l.voxel_pos_1.x ,2) + Math.pow(l.voxel_pos_0.y - l.voxel_pos_1.y ,2) + Math.pow(l.voxel_pos_0.z - l.voxel_pos_1.z ,2) )

			          total_cable_length +=  cablelength
			        } catch (err){
			          console.error(err)
			        }
			      }
			      logiccounter++
			      handleAnalyzingProgress(CATEGORY_LOGIC_CONNECTIONS, logiccounter / logiclength)
			    } else if (v.logic_node_links && v.logic_node_links.logic_node_link && v.logic_node_links.logic_node_link.voxel_pos_0 ){
			      logiccounter = 1
			    }

			    handleAnalyzingProgress(CATEGORY_LOGIC_CONNECTIONS, 1)
			    setTimeout(fulfill, 1)
			})
	    }


		let total_mass = 0
		let single_masses = {}
		let combined_masses = {}
		let total_relative_masses = {}

	    function _prepare_mass(){
	    	return new Promise((fulfill, reject)=>{
				handleAnalyzingProgress(CATEGORY_POST_PROCESSING_MASSES, 0)

				for(let c of Object.keys(components)){
					if (c === 'microprocessor') {
						continue
					}
					let mapped_mass = map('components', c).mass
					let mass = typeof mapped_mass === 'number' ? mapped_mass : 0
					single_masses[c] = mass
					let combined_mass = components[c] * mass
					if( typeof combined_mass !== 'number' || isNaN(combined_mass)){
						combined_mass = 0
					}
					combined_masses[c] = combined_mass
					total_mass += combined_mass
				}

				let mp_mass = 0
				for (let mp of microprocessors) {
					mp_mass += (mp.width || 1) * (mp.length || 1)
				}
				single_masses['microprocessor'] = v.data_version === '2' ? 1 : '?'// version 3 has mass depending on size of the component
				combined_masses['microprocessor'] = mp_mass
				total_mass += mp_mass

				for(let c of Object.keys(components)){
					total_relative_masses[c] = total_mass === 0 ? 0 : (combined_masses[c] / total_mass)
				}

			    handleAnalyzingProgress(CATEGORY_POST_PROCESSING_MASSES, 1)
			    setTimeout(fulfill, 1)
			})
	    }


	    function _prepare_components(){
	    	return new Promise((fulfill, reject)=>{
	    		handleAnalyzingProgress(CATEGORY_POST_PROCESSING_COMPONENTS, 0)
			    let componentslength = Object.keys(components).length
			    let componentscounter = 0
			    for(let c of Object.keys(components) ){
			      data.components.push({
			        name: c,
			        label: map('components', c).label,
			        image: map('components', c).image,
			        count: components[c],
			        single_mass: single_masses[c],
			        total_relative_mass: parseInt(total_relative_masses[c] * 10000) / 100 + '%'
			      })
			      componentscounter++
			      handleAnalyzingProgress(CATEGORY_POST_PROCESSING_COMPONENTS, componentscounter / componentslength)
			    }

			    data.components.sort((a, b)=>{
			      if(a.name === b.name){
			        return 0
			      }
			      return a.count > b.count ? -1 : 1
			    })
	    		handleAnalyzingProgress(CATEGORY_POST_PROCESSING_COMPONENTS, 1)
	    		setTimeout(fulfill, 1)
	    	})
	    }


	    function _prepare_logics(){
	    	return new Promise((fulfill, reject)=>{
				handleAnalyzingProgress(CATEGORY_POST_PROCESSING_LOGIC_CONNECTIONS, 0)
			    let logicslength = Object.keys(logics).length
			    let logicscounter = 0
			    for(let l of Object.keys(logics) ){
			      data.logics.push({
			        type: l,
			        label: map('logics', l).label,
			        image: map('logics', l).image,
			        count: logics[l]
			      })
			      logicscounter++
			      handleAnalyzingProgress(CATEGORY_POST_PROCESSING_LOGIC_CONNECTIONS, logicscounter / logicslength * 0.9)
			    }

			    data.logics.sort((a, b)=>{
			      if(a.type === b.type){
			        return 0
			      }
			      return a.count > b.count ? -1 : 1
			    })
			    handleAnalyzingProgress(CATEGORY_POST_PROCESSING_LOGIC_CONNECTIONS, 1)
			    setTimeout(fulfill, 1)
	    	})
	    }


	    function _prepare_colors(){
	    	return new Promise((fulfill, reject)=>{
				handleAnalyzingProgress(CATEGORY_POST_PROCESSING_COLORS, 0)
			    let colorslength = Object.keys(colors).length
			    let colorscounter = 0
			    for(let k of Object.keys(colors)){
			      data.colors.push({hex: k, rgb: hexToRgb(k), color: '#'+k.replace('#', ''), amount: colors[k]})
			      colorscounter++
			      handleAnalyzingProgress(CATEGORY_POST_PROCESSING_COLORS, colorscounter / colorslength * 0.9)
			    }
			    data.colors.sort((a, b)=>{
			      if(a.amount === b.amount){
			        return 0
			      }
			      return a.amount > b.amount ? -1 : 1
			    })
			    handleAnalyzingProgress(CATEGORY_POST_PROCESSING_COLORS, 1)
			    setTimeout(fulfill, 1)
	    	})
	    }


	    function _prepare_microprocessors(){
	    	return new Promise((fulfill, reject)=>{
	    		handleAnalyzingProgress(CATEGORY_POST_PROCESSING_MICROCONTROLLERS, 0)
			    let microprocessorslength = microprocessors.length
			    let microprocessorscounter = 0
			    for(let mp of microprocessors){
			      data.microcontrollers.push({
			        name: mp.name,
			        description: mp.description,
			        components_count: mp.group && mp.group.components && mp.group.components.c instanceof Array ? mp.group.components.c.length : 0,
			        inout_count: mp.nodes && mp.nodes.n instanceof Array ? mp.nodes.n.length : 0
			      })
			      microprocessorscounter++
			      handleAnalyzingProgress(CATEGORY_POST_PROCESSING_MICROCONTROLLERS, microprocessorscounter / microprocessorslength * 0.9)
			    }
			    data.microcontrollers.sort((a, b)=>{
			      if(a.components_count === b.components_count){
			        if(a.inout_count === b.inout_count){
			          return 0
			        }
			        return a.inout_count > b.inout_count ? -1 : 1
			      }
			      return a.components_count > b.components_count ? -1 : 1
			    })
			    handleAnalyzingProgress(CATEGORY_POST_PROCESSING_MICROCONTROLLERS, 1)
			    setTimeout(fulfill, 1)
	    	})
	    }




	    let authors = ""
	    if(authors && typeof authors === 'object')
	    for(let a of v.authors){
	    	authors += a.username
	    }

	    let stats = {}

	    function _statistics(){
	    	return new Promise((fulfill, reject)=>{
				handleAnalyzingProgress(CATEGORY_STATISTICS, 0)

				stats = {
			      data_version: v.data_version,
			      is_advanced: v.is_advanced === "true",
			      is_static: v.is_static === "true",
			      authors: authors,
			      body_count: v.bodies.body.length,
			      component_count: component_count,
			      total_mass: total_mass,
			      logics_count: v.logic_node_links.logic_node_link instanceof Array? v.logic_node_links.logic_node_link.length : 0,
			      color_count: data.colors.length,
			      microcontroller_count: data.microcontrollers.length,
			      total_cable_length: parseInt(total_cable_length / 4.0)
			    }

			    let statslength = Object.keys(stats).length
			    let statscounter = 0
			    for(let k of Object.keys(stats) ){
			    	let label = map('general_stats', k)
			      data.general.push({
			        name: k,
			        label: typeof label === 'string' ? label : label.label,
			        value: stats[k],
			        checkmark: typeof stats[k] === 'boolean'
			      })
			      statscounter++
			      handleAnalyzingProgress(CATEGORY_STATISTICS, statscounter / statslength)
			    }
			    handleAnalyzingProgress(CATEGORY_STATISTICS, 1)
	    		setTimeout(fulfill, 1)
	    	})
	    }

	    _components()
	      .then(_logic_connections)
	      .then(_prepare_mass)
	      .then(_prepare_components)
	      .then(_prepare_logics)
	      .then(_prepare_colors)
	      .then(_prepare_microprocessors)
	      .then(_statistics)
	      .then(()=>{
	    	fulfill( TMPL('stats', data) )
	    })
  	})
  }

  /* cuts of the tho hex chars for opacity 000000>FF< */
  function cleanColor(hex){
    if(typeof hex !== 'string'){
      return ''
    }
    if(hex.length > 8){
      return hex.toLowerCase()
    }
    if(hex.length === 8){
      return hex.substring(0,6).toLowerCase()
    }
    if(hex.length === 3){
      // Expand shorthand form (e.g. "03F") to full form (e.g. "0033FF")
      let shorthandRegex = /^#?([a-f\d])([a-f\d])([a-f\d])$/i;
      return hex.replace(shorthandRegex, function(m, r, g, b) {
          return r + r + g + g + b + b;
      }).toLowerCase()
    }

    return hex.toLowerCase()
  }

  function hexToRgb(hex) {
    if(typeof hex !== 'string'){
      return ''
    }


    var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
    /*return result ? {
        r: parseInt(result[1], 16),
        g: parseInt(result[2], 16),
        b: parseInt(result[3], 16)
    } : null;*/
    return result ? ( parseInt(result[1], 16) + ', ' + parseInt(result[2], 16) + ', ' + parseInt(result[3], 16) ) : ''
  }

  function fullRgbToHex(r,g,b){
    if(typeof r !== 'number' || typeof g !== 'number' || typeof b !== 'number')
    try {
      let rr = parseInt(r)
      let gg = parseInt(g)
      let bb = parseInt(b)
      return rgbToHex(rr) + rgbToHex(gg) + rgbToHex(bb)
    } catch (ex){
      return false
    }
  }

  function rgbToHex(number){
    var hex = Number(number).toString(16);
    if (hex.length < 2) {
         hex = "0" + hex;
    }
    return hex;
  }


  /* Mapping */

  function map(namespace, key){
    if(MAPPINGS[namespace] && MAPPINGS[namespace][key]){
      return MAPPINGS[namespace][key]
    } else {
      return {label: '?' + key + '?'}
    }
  }


  /* Helpers */


  function startAnalyzingProgress(){
    $('#analyze-progress').show()
    $('#analyze-progress .category').html(CATEGORY_LABELS[0])
    $('#analyze-progress .progress').html('0 %')
    $('#analyze-progress .step').html('1/' + CATEGORY_LABELS.length)
  }

  function stopAnalyzingProgress(){
    $('#analyze-progress').hide()
    $('#analyze-progress .category').html('')
    $('#analyze-progress .progress').html('')
    $('#analyze-progress .step').html('')
  }

  function handleAnalyzingProgress(category, progress){
    $('#analyze-progress .category').html(CATEGORY_LABELS[category])
    $('#analyze-progress .progress').html( parseInt(progress * 100.0) + ' %' )
    $('#analyze-progress .step').html(category+1 + '/' + CATEGORY_LABELS.length)
  }


  function handleFileError(evt){
    console.error(evt)
    $(result_container).html(evt.toString())
  }

  function handleFileProgress(evt){
    if(evt === true){
      handleAnalyzingProgress(CATEGORY_LOADING_FILE, 1)
      $(result_container).css('cssText', '')
      return
    }
    handleAnalyzingProgress(CATEGORY_LOADING_FILE, evt.loaded / evt.total)
    let percent = parseInt(evt.loaded / evt.total * 10000) / 100.0
    $(result_container).css('background', 'linear-gradient(to right, blue 0%, blue ' + percent + '%, white ' + (percent+0.01) + '%, white 100%)')
  }

  function log(msg){
    let args = []
    for(let a of arguments){
      args.push(a)
    }
    console.log.apply(console, ['ModelAnalyzer:'].concat(args))
  }




  global.ModelAnalyzer = {
    analyzeBlob: analyzeBlob
  }

})(window, jQuery)
